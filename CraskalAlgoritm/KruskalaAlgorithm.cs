﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CraskalAlgoritm
{
    public class KruskalAlgorithm
    {
        public static List<Edge> Kruskal(int numVertices, List<Edge> edges)
        {
            List<Edge> minimumSpanningTree = new List<Edge>();
            edges.Sort();

            int[] parents = new int[numVertices];
            for (int i = 0; i < numVertices; i++)
            {
                parents[i] = i;
            }

            foreach (Edge edge in edges)
            {
                int parent1 = Find(parents, edge.Source);
                int parent2 = Find(parents, edge.Destination);

                if (parent1 != parent2)
                {
                    minimumSpanningTree.Add(edge);
                    parents[parent1] = parent2;
                }
            }

            return minimumSpanningTree;
        }

        private static int Find(int[] parents, int vertex)
        {
            if (parents[vertex] != vertex)
            {
                parents[vertex] = Find(parents, parents[vertex]);
            }

            return parents[vertex];
        }
    }
}
