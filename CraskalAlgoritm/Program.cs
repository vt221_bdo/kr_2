﻿using System;
using System.Collections.Generic;
namespace CraskalAlgoritm
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Edge> edges = new List<Edge>();
            edges.Add(new Edge(0, 1, 4));
            edges.Add(new Edge(0, 7, 8));
            edges.Add(new Edge(1, 7, 11));
            edges.Add(new Edge(1, 2, 8));
            edges.Add(new Edge(7, 8, 7));
            edges.Add(new Edge(7, 6, 1));
            edges.Add(new Edge(2, 8, 2));
            edges.Add(new Edge(2, 3, 7));
            edges.Add(new Edge(2, 5, 4));
            edges.Add(new Edge(6, 8, 6));
            edges.Add(new Edge(6, 5, 2));
            edges.Add(new Edge(3, 5, 14));
            edges.Add(new Edge(3, 4, 9));
            edges.Add(new Edge(5, 4, 10));

            List<Edge> minimumSpanningTree = KruskalAlgorithm.Kruskal(9, edges);

            foreach (Edge edge in minimumSpanningTree)
            {
                Console.WriteLine(edge.Source + " - " + edge.Destination + " : " + edge.Weight);
            }

        }
    }
    
}




